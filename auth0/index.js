const fetch = require('node-fetch');
const atob = require('atob');
class Auth0Handler {
    constructor() {
        this.token = null
        this.decoded = null
        this.attempts = 0 //  TODO - maybe track 401s and reattempt on fail?
    }
    /**
     * Gets a new token from Auth0 and stores it in the object
     * this will throw an error if an invalid access token is returned
     * @returns {Object} - An authentication token 
     */
    async getToken() {
        try {
            const credentials = require(process.env.AUTH0_CREDENTIALS)
            const url = `${process.env.AUTH0_URI}/oauth/token`
            let response = await fetch(url, {
                method: 'post',
                body: JSON.stringify(credentials),
                headers: { 'Content-Type': 'application/json' },
            })
            let data = await response.json()
            if (typeof data.access_token === 'undefined') throw new Error('[-] Malformed token response')
            this.token = data
            this.decoded = JSON.parse(atob(data.access_token.split('.')[1]))
            console.log('[+] Retrieved new Auth0 bearer token')
            return this.token
        } catch (error) {
            throw error
        }
    }

    /**
     * Check if the token is expired just by looking at the value of the expiry datetime
     * @returns {Boolean} true - if the token was expired
     * @throws if the token could not be set/refreshed
     * TODO - doesn't to 401 validation
     */
    async validateToken() {
        try {
            const timenow = Math.floor(new Date() / 1000)
            if (this.decoded === null) await this.getToken()
            if (timenow >= this.decoded.exp) await this.getToken()    
            if (timenow < this.decoded.exp) return true
            throw new Error('[-] Could not retrieve valid Auth0 JWT')
        } catch(error) {
            throw error
        }
    }

    /**
     * Retrieve all the users from Auth0
     * @param {Array} users - All the users
     * @throws If the token is malformed or the resulting api call fails
     */
    async getUsers() {
        try {
            await this.validateToken()
            const url = `${process.env.AUTH0_URI}/api/v2/users`
            let response = await fetch(url, {
                method: 'get',
                headers: {
                    'Authorization': `Bearer ${this.token.access_token}`,
                }
            })
            let data = await response.json()
            return data
        } catch (error) {
            //  TODO catch bad status codes as errors and handle here
            throw error
        }
    }

    async getUser(id) {
        try {
            await this.validateToken()
            const url = `${process.env.AUTH0_URI}/api/v2/users/` + id
            let response = await fetch(url, {
                method: 'get', 
                headers: {
                    'Authorization': `Bearer ${this.token.access_token}`
                }
            })
            let data = await response.json()
            return data
        } catch (error) {
            throw error
        }
    }

    /**
     * Updates the array of meta data which is actually the list of Client Portal IDs
     * @param {String} userId - Make sure the 'auth|' string is appended before the ID ! ie: auth|xxxxxxxxx
     * @param {Array} dealerships - The list of VMS assigned dealers as strings, or a '*' character
     * for all dealers
     * @throws If something fails
     */
    async updateUserMetaDataVMSAssignedDealers(userId, dealerships) {
        try {
            await this.validateToken()
            const url = `${process.env.AUTH0_URI}/api/v2/users/auth0|${userId}`
            const payload = {
                user_metadata: {
                    vms_assigned_dealers: dealerships
                }
            }
            let response = await fetch(url, {
                method: 'patch',
                body: JSON.stringify(payload),
                headers: {
                    'Authorization': `Bearer ${this.token.access_token}`,
                    'Content-Type': 'application/json'
                }
            })
            let data = await response.json()
            return data
        } catch (error) {
            //  TODO catch bad status codes as errors and handle here
            throw error
        }
    }

}
module.exports = { Auth0Handler }