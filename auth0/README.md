## Auth**0**Handler

### Description

This auth0 module handles requests via through stored client credentials to the auth0 API to make requests to auth0 to manage users.

The handler handles the machine to machine connection via the client credentials being traded for a token that is used to make requests to the auth0 API.

### Current feature set

Currently, the module handles:

1. Obtaining an array of users
2. Obtaining a specific user via their auth0 user_id
3. Updating a user's list of dealerships they own through an array of ids mapped to dealerships that live in MongoDB

### Future feature set

1. Add users and patch users for other general data besides dealerships
2. Implement permissions and checks on the above by field

### Prerequisites for running

You will need:

1. A set of Auth0 Credentials with a client id, client secret, audience, and grant type retrieved from auth0
2. An auth0 domain for hitting the auth0 api