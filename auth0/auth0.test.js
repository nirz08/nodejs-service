describe('verifies auth0 credentials are valid for server instance', () => {

    var auth

    beforeAll(() => {
        require('dotenv').config({ path: 'config/.env' })
        var {Auth0Handler} = require('./index')
        auth = new Auth0Handler()
    })

    afterAll(async () => {
        await new Promise(resolve => setTimeout(() => resolve(), 500)); // avoid jest open handle error
    })

    it('Makes sure a token is received based on environment credentials', async () => {
        let token = await auth.getToken()
        expect(token).toBeDefined()
    })

    it('Detects a null or undefined token and gets a token through validateToken', async () => {
        var isValidated = await auth.validateToken()
        expect(isValidated).toBe(true)
    })

    it('Gets an array of users from Auth0 after validating', async () => {
        var data = await auth.getUsers()
        expect(data.length).toBeGreaterThan(1)
    })

    it('Gets a user', async () => {
        var user = await auth.getUser('auth0|5e542f15ad27670e6807077d')
        expect(user.email).toBe('nirzinger@test.com')
    })

})