# Use our private docker image node.js with aws cli
FROM node:10-slim

# Create app directory with the config folder
RUN mkdir -p /usr/src/app/config
WORKDIR /usr/src/app
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./

# Install pm2-runtime globally before installing dependancies
RUN npm install pm2 -g
RUN npm install

# If you are building your code for production
# RUN npm ci --only=production
ARG NODE_ENV
# Bundle app source
COPY . .
RUN cp -a -r /usr/src/app/share/. /usr/src/app/config/
RUN if [ "$NODE_ENV" = "production" ] || [ "$NODE_ENV" = "development" ] || [ "$NODE_ENV" = "stage" ] ; then \
    npm run test ; \
    fi ;
RUN if [ $? = 1 ] ; then exit 1 ; fi
# This is the port that application can be accessed at
EXPOSE 8080

# Run using pm2-runtime and set output logs to json format
ENTRYPOINT ["pm2-runtime", "app.js"]