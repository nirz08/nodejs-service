
module.exports = (async (test = false) => {
    // Load the env file from the config folder
    const fs = require('fs')
    if (fs.existsSync('config/.env')) {
        const dotenv = require('dotenv')
        const result = dotenv.config({ path: 'config/.env' })
        console.log(`[+] Loaded .env file`);
    } else {
        throw new Error('Missing config directory')
    }
    const express = require('express')
    const connectionInitializer = require('./database')
    const connections = await connectionInitializer()
    console.log(`[+] Database connection successful`)
    // const serviceRoutes = require('./messaging/service');
    let app = express()
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    const apiRoutes = require('./messaging/api_v0');
    // const serviceRoutes = require('./messaging/service');
    if(test) {
        apiRoutes(app, connections.test)
    } else {
        apiRoutes(app, connections.apiGateway)
    }
    return app
})