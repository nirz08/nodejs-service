(async function main() {
    const fs = require('fs')
    if (fs.existsSync('config/.env')) {
        const dotenv = require('dotenv')
        const result = dotenv.config({ path: 'config/.env' })
        console.log(`[+] Loaded .env file`);
    } else {
        throw new Error('')
    }
    //  Attempt to make database connections
    //  TODO if these fail it probably shouldn't start the express server...

    //  Start express server
    //  TODO express error handling
    const PORT = process.env.APP_PORT
    const HOST = process.env.APP_HOST
    let app = await require('./server')()
    app.listen(PORT, HOST, () => {
        console.log(`[+] Messaging API started on: ${HOST}:${PORT}`);
    });

})()
