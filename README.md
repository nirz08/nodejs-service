# dealership-management

## Description:
The Dealership Management service acts as a middleware between the Auth0 user store, the dealer database store resource and the API gateway.

Its purpose is to serve endpoints for modifying dealers and linking user accounts to dealers. It also handles transformation and validation of
objects into a uniform payload between the DealerPortal front end and the database/Auth0 store.

## Domain Model
Dealership Model (Ref. the odin_dealers collection in the the seo_2 database for PATCH/POST requests)
```
{  
    "_id" : ObjectId,
    "user_id" : Integer,
    "_lastUpdated" : ISODate,
    "dealer_address" : String,
    "dealer_business_name" : String,
    "dealer_city" : String,
    ...
}  
```

User Model (Ref. the Auth0 user store, found in GET response body)
```
{
    "created_at": "2020-02-07T20:14:29.333Z",
    "email": "nirzinger@test.com",
    "email_verified": Boolean,
    "family_name": String,
    "given_name": String,
    "identities": [
        {
            "connection": "dealerportal-dev",
            "user_id": "5e3dc525e235180e9dda3196",
            "provider": "auth0",
            "isSocial": false
        }
    ],
    "name": String,
    "nickname": String,
    "user_id": "auth0|xxxxxxxxxxxxxxxxxxxxxxxx",
    "user_metadata": {
        "roles": [
            String
        ],
        "vms_assigned_dealers": [
            String
        ]
    },
    "username": String,
    ...
}
```

Linking User-Dealership Model (Payload for Auth0 User PATCH requests)
```
{
    dealerships: [
        String
    ]
}
```

## Entities  
**Database Connection** (MongoDB):

You can replace this with another database handler if you wish, but for running straight out of the box:  
+ *Attributes* (ENV variables which need to be loaded at atleast runtime):  
    - `MONGO_DB_CLOUD_CREDENTIALS` - Location of a json file with properties URI (mongoDB URI), and DB (mongoDB database string)  
    - `MONGO_DB_POOLSIZE`  
    - `MONGO_DB_COLLECTION` - Specific collection used for read/write  
    - `APP_PORT`  
    - `APP_HOST`  

There is a test DB that can be found under db.test after importing and initializing the DB. Simply pass a boolean value of true
to the initialization of the server and it will use the test db as default.

## Messaging 
Communication occurs through an exposed public API endpoints that accepts queries for GET/POST/PATCH/DELETE on dealerships and GET/PATCH Auth0 users. 

**Auth0**:

Ensure the client credentials `./config/auth0.json` file and the `AUTH0_CREDENTIALS` and `AUTH0_URI` are defined in the `./config/.env` file 

## File System
None, but will reference a `config` directory which will contain all application credentials and secrets for configuration. Likely will be mounted as a shared volume
inside the parent node of the container if running in Docker. 

---

## How to Run the Service:

### Dev Machine (Bare Metal):
1. Copy the .env file and config folder into the root directory 
2. `npm install && npm run dev`
3. Navigate to http:/localhost:8080 to see the last predefined number of documents stored in the MongoDB collection

### Dev Machine (Docker):
1. Copy the .env file and config folder into the root directory
2. `docker build --force-rm -t <dockerID>/notification-handler .`
3. `docker run -v $PWD/config:/usr/src/app/config --env-file .env -p 49160:8080 <dockerID>/notification-handler:latest`