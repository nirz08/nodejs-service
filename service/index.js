/**
 * TODO
 * document schema for data, priority function, user functions, handle different message types,
 * create titles for notifications, decide if source should be appended here or in the sending service,
 * move transformations into their own file?
 */
const dealershipModel = require('../models/dealership.json')
/**
 * Transforms the payload to return only where the payload fields are a proper subset of the model
 * @param {Array} transform - A list of formatted data objects from various message handlers 
 * @returns An object representing a dealership that conforms to the model document schema
 * @throws Something bad
 */
function transformDealerGET(data) {
    try {
        //  These are all the fields that are allowed in the document model
        const allowed = Object.keys(dealershipModel)
        //  Merge the model and data objects together ensuring the data objects's 
        //  values are preserved in the returned unfiltered object
        const unfiltered = {...dealershipModel, ...data}
        //  Filter out all keys that are not in the model
        const filtered = Object.keys(unfiltered)
            .filter(key => allowed.includes(key))
            .reduce( (obj, key) => {
                obj[key] = unfiltered[key]
                return obj
            }, {})
            console.log(filtered)
        return filtered
    } catch (error) {
        throw error;
    }
}

/**
 * Transforms the payload to return only where the payload fields are a proper subset of the model
 * @param {Array} transform - A list of formatted data objects from various message handlers 
 * @returns An object representing a dealership that conforms to the model document schema
 * @throws Something bad
 */
function transformDealerPOST(data) {
    try {
        //  These are all the fields that are allowed in the document model
        const allowed = Object.keys(dealershipModel)
        //  Merge the model and data objects together ensuring the data objects's 
        //  values are preserved in the returned unfiltered object
        const unfiltered = {...dealershipModel, ...data}
        //  Filter out all keys that are not in the model
        const filtered = Object.keys(unfiltered)
            .filter(key => allowed.includes(key))
            .reduce( (obj, key) => {
                obj[key] = data[key]
                return obj
            }, {})
        //  There's no need to keep the _id field from mongoDB in here
        delete filtered["_id"]
        delete filtered["user_id"]
        return filtered
    } catch (error) {
        throw error;
    }
}

   /**
   * Middleware function validates the data from PATCH and POST requests to ensure
   * 1. Each key in the data exists in the model
   * 2. The data types for each key match the model
   * 3. For POST requests a certain subset of keys from the model exist in the data
   * @param {*} req Request object
   * @param {*} res Response object
   * @param {*} next 
   */
   function validateDealer(req, res, next) {
    try {
      const data = req.body
      for(const [key, val] of Object.entries(data)) {
        //  Make sure the key exists in the model
        if(typeof dealershipModel[key] === 'undefined') {
          return res.status(401).send({error: `Invalid odin_dealer key: ${key}`})
        }
        //  Make sure the data type for that key matches the model 
        if(typeof dealershipModel[key] !== typeof val) {
          return res.status(401).send({error: `Invalid odin_dealer value data type: ${key}`})
        }
      }
      // The data MUST have these keys if the request is a POST - is this the best place to put it...?
      if(req.method == 'POST') {
        if(!data['dealer_business_name']) {
          return res.status(401).send({error: `Missing odin_dealer dealer_business_name`})
        }
      }
      //  Everything passed validation so keep going
      next()
    } catch (error) {
      console.log(error)
      return res.status(500).send({error: `dealerValidator failed`})
    }
  }

module.exports = { transformDealerGET, transformDealerPOST, validateDealer }