## MongoDB DB Connection

This database module connects to three DBs, one for service to service communication, one for public endpoints, and a test DB.

It creates one connection for each that persists for the lifetime of the service and will be destroyed when mongoDB atlas times out the connection (?)