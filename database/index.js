const MongoClient = require('mongodb').MongoClient;
const credential = require(process.env.MONGO_DB_CLOUD_CREDENTIALS); 

const MONGO_URI = credential.URI
const MONGO_DB = credential.DB
const POOLSIZE = process.env.MONGO_DB_POOLSIZE

function connect(url) {
  return MongoClient.connect(url, { 
    useNewUrlParser : true,
    useUnifiedTopology: true,
    poolSize: POOLSIZE
  }).then(client => client.db(MONGO_DB))
}
 
function connectToTest(url) {
  return MongoClient.connect(url, { 
    useNewUrlParser : true,
    useUnifiedTopology: true,
    poolSize: POOLSIZE
  }).then(client => client.db('test'))
}

module.exports = async function() {
  let databases = await Promise.all([connect(MONGO_URI), connect(MONGO_URI), connectToTest(MONGO_URI)])
  
  return {
    apiGateway: databases[0],
    messaging: databases[1],
    test: databases[2]
  }
}