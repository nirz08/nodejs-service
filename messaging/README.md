## API V0

The module for v0 contains a function that accepts a db connection and the express application instance to serve the routes on.

It requires a collection string that points to a collection of dealers to function properly that matches up with however your user store or repository uses to connect users and dealerships together.

For testing the routes are loaded with the test db collection enabled on the mongoDB atlas. Please readjust or create a new database module in the database folder to interface with other DBs.

### Functions Overview
1. Global middleware - Filters for parameters and checks auth0 stuff
2. GET Dealerships - Self explanatory
3. GET OEM Data - Gets a bunch of dealerships based on OEM
4. dealershipcheck - validates data for POST and PATCH
5. POST Dealerships - creates a new dealership
6. GET Dealership - by ObjectId
7. PATCH dealership - Patches a specific dealership
8. DELETE dealership - Deletes a specific dealership by ObjectId
9. GET users - gets multiple auth0 users
10. PATCH user - patches by auth0 ID

### Testing

------

Most of these tests just test for simple and expected inputs and outputs on the API call itself. 

Unfortunately, you'll have to add a user into the header for each request or you will be denied (unless you want to have a certain route opened up or something). 

Please see the description of each individual test for what it does - it's pretty self explanatory.