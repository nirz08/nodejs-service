/**
 * This is a route module that uses a depenency injection pattern to load
 * the routes onto the express server for the api gateway using REST.
 */
module.exports = function (app, db) {
  const { Auth0Handler } = require('../auth0')
  const auth0 = new Auth0Handler()
  const service = require('../service')
  var ObjectId = require('mongodb').ObjectId
  const aqp = require('api-query-params')
  const MONGO_DB_COLLECTION = process.env.MONGO_DB_COLLECTION
  collection = db.collection(MONGO_DB_COLLECTION)

  //Global middleware on express level
  app.use(async (req, res, next) => {
    try {
      let user_id = req.headers.sub
      let { filter, skip, limit, sort, projection } = aqp(req.query)
      req.filter = typeof filter === 'undefined' ? {} : filter
      req.limit = typeof limit === 'undefined' ? 0 : limit
      req.skip = typeof skip === 'undefined' ? 0 : skip
      req.sort = typeof sort === 'undefined' ? '' : sort
      req.projection = typeof projection === 'undefined' ? {} : projection
      if (!(user_id.search('auth0') + 1)) {
        //do handling for machine to machine application
        //next()
        //for now, throw that shit 
        throw(new Error('Not a valid user'))
      } else {
        //do handling for user
        let user = await auth0.getUser(user_id)
        // console.log(user)
        if(user.statusCode == 404) {
          throw new Error("No user exists for given requester")
        }
        req.user = user
        next()
      }
    } catch (error) {
        res.status(error.status ? 500 : 401)
        console.log(error)
        next(error)
    }
  })


  /**
   * This endpoint returns a list of dealers depending on what role the requester has. 
   * TODO: Machine to machine handling for GET requests
   * PARAMS:
   * Parameters are caught at the global express middleware handler written above. Please see above for what fields are accepted in the handler.
   */
  app.get('/api/v0/dealerships', async (req, res, next) => {
    try {
      const user = req.user
      if (!(user.user_metadata.roles.find((elem) => { return elem == 'administrator' || elem == 'test_employee' }) + 1)) {
        const ids = user.user_metadata.vms_assigned_dealers.map((element) => {
          return ObjectId(element)
        })
        req.filter["_id"] = { $in: ids }
      }
      const result = await collection.find(req.filter, { projection: req.projection }).skip(req.skip).limit(req.limit).sort(req.sort).toArray()
      const dealers = result.map(dealer => service.transformDealerGET(dealer))
      res.json(dealers)
    } catch (error) {
      console.log(error)
      next(error)
    }
  })

  /**
   * Get OEM data
   */
  app.get('/api/v0/dealerships/oem', async (req, res, next) => {
    try {
      let data = await collection.aggregate([
        {
          $group: {
            _id: "$dealer_oem", dealers: {
              $push: {
                _id: "$_id",
                dealer_business_name: "$dealer_business_name",
                organic_gmb_oem_cta_link: "$organic_gmb_oem_cta_link",
                organic_gmb_oem_active: "$organic_gmb_oem_active",
                organic_gmb_locations: "$organic_gmb_locations",
              }
            }
          }
        }
      ]).toArray();
      res.send(data)
    } catch (error) {
      console.log(error)
      next(error)
    }
  })

  /**
   * Create a new dealership objects
   * TODO - there should be validation on business name
   */
  app.post('/api/v0/dealerships', service.validateDealer, async (req, res, next) => {
    const remoteAddress = req.connection.remoteAddress
    try {
      payload = req.body
      //  Transform the dealership payload to conform with the schema
      const dealership = service.transformDealerPOST(payload)
      
      const found = await collection.findOne({dealer_business_name: dealership.dealer_business_name})
      if(found) {
        throw new Error('Dealership already exists')
      }
      const result = await collection.insertOne(dealership)
      const insertedCount = result.insertedCount
      await collection.updateOne({_id: ObjectId(result.insertedId)}, {$set: {user_id: result.insertedId}})
      //  Throw if mongodb insert failed in some way 
      if (typeof insertedCount === 'undefined') throw new Error(JSON.stringify(result))

      console.log(`[+] Inserted ${insertedCount} dealerships from ${remoteAddress}`)
      const response = {
        insertedCount: insertedCount,
        _id: result.insertedId
      }
      res.send(response)
    } catch (error) {
      console.log(`[+] Failed to insert dealerships from ${remoteAddress}`)
      //  just do this for now, error handling will probably need to be standardized for all service message passing... 
      //  and only pass back an error message that makes sense for what happened
      res.status(500)
      res.json({
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        recieved: req.body
      })
    }
  })
  /**
    * Returns an array of dealership objects or a specific dealership given the id.
    * Additionally, any of the filter, skip, limit, sort, projection or population 
    * commands from mongoDB will work if passed as query parameters
    * see: https://www.npmjs.com/package/api-query-params
    */
  app.get('/api/v0/dealerships/:id', async (req, res, next) => {
    const remoteAddress = req.connection.remoteAddress
    try {
      const id = req.params.id
      const result = await collection.findOne({ _id: ObjectId(id) })
      const dealership = service.transformDealerGET(result)
      res.send(dealership)
    } catch (error) {
      console.log(`[+] Failed to get dealerships from ${remoteAddress}`)
      //  just do this for now, error handling will probably need to be standardized for all service message passing... 
      //  and only pass back an error message that makes sense for what happened
      res.status(500)
      res.json({
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        recieved: req.body
      })
    }
  })
  /**
   * Update a single dealership object
   * TODO - there should be a validation of if the dealership already exists in the database?
   */
  app.patch('/api/v0/dealerships/:id', service.validateDealer, async (req, res, next) => {
    const remoteAddress = req.connection.remoteAddress
    try {
      const id = req.params.id
      const dealership = req.body
      const result = await collection.updateOne({ _id: ObjectId(id) }, { $set: dealership })
      const modifiedCount = result.modifiedCount
      //  Throw if mongodb insert failed in some way 
      if (typeof modifiedCount === 'undefined') throw new Error(JSON.stringify(result))
      console.log(`[+] Updated ${modifiedCount} dealership from ${remoteAddress}`)
      const response = {
        modifiedCount: modifiedCount,
      }
      res.send(response)
    } catch (error) {
      console.log(`[+] Failed to edit dealership from ${remoteAddress}`)
      //  just do this for now, error handling will probably need to be standardized for all service message passing... 
      //  and only pass back an error message that makes sense for what happened
      res.status(500)
      res.json({
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        recieved: req.body
      })
    }
  })

  /**
   * Update a single dealership object
   * TODO - there should be a validation of if the dealership already exists in the database?
   */
  app.delete('/api/v0/dealerships/:id', async (req, res, next) => {
    const remoteAddress = req.connection.remoteAddress
    const id = req.params.id
    try {
      const result = await collection.deleteOne({ _id: ObjectId(id) })
      const deletedCount = result.deletedCount
      //  Throw if mongodb insert failed in some way 
      if (typeof deletedCount === 'undefined') throw new Error(JSON.stringify(result))
      console.log(`[+] Removed ${deletedCount} dealerships from ${remoteAddress}`)
      const response = {
        nRemoved: deletedCount
      }
      res.send(response)
    } catch (error) {
      console.log(`[+] Failed to edit dealership from ${remoteAddress}`)
      //  just do this for now, error handling will probably need to be standardized for all service message passing... 
      //  and only pass back an error message that makes sense for what happened
      res.status(500)
      res.json({
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        recieved: req.body
      })
    }
  })

  app.get('/api/v0/users', async (req, res, next) => {
    const remoteAddress = req.connection.remoteAddress
    try {
      const result = await auth0.getUsers()
      res.send(result)
    } catch (error) {
      console.log(`[+] Failed to get Auth0 users from ${remoteAddress}`)
      //  just do this for now, error handling will probably need to be standardized for all service message passing... 
      //  and only pass back an error message that makes sense for what happened
      res.status(500)
      res.json({
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        recieved: req.body
      })
    }
  })

  /**
  * Updates a Auth0 user's metadata field with a new value for the vms_assigned_dealers key
  * the request body should contain an array of strings for the dealership client portal ids
  */
  app.patch('/api/v0/users/:id', async (req, res, next) => {
    const remoteAddress = req.connection.remoteAddress
    let id = req.params.id
    try {
      const dealerships = req.body.dealerships
      //  Validate the dealerships object before inserting it into the database - TODO?
      const result = await auth0.updateUserMetaDataVMSAssignedDealers(id, dealerships)
      res.send(result)
    } catch (error) {
      console.log(`[+] Failed to update Auth0 user metadata from ${remoteAddress}`)
      //  just do this for now, error handling will probably need to be standardized for all service message passing... 
      //  and only pass back an error message that makes sense for what happened
      res.status(500)
      res.json({
        error: JSON.stringify(error, Object.getOwnPropertyNames(error)),
        recieved: req.body
      })
    }
  })

  return app
}