
describe('api_v0_tests', () => {
    var server
    var supertest = require('supertest')
    var request
    const user_id = 'auth0|5e542f15ad27670e6807077d' // test-dev Noah
    beforeAll(async () => {
        server = await require('../server')(true)
        request = supertest(server)
    })
    afterAll(async () => {
        await server.close()
        await new Promise(resolve => setTimeout(() => resolve(), 500)); // avoid jest open handle error
    })

    it('POSTS a new dealership, tries to post the same dealership and fails, patches original dealership, and then deletes it', async () => {
        let data = {
            "dealer_business_name": "Test Business", // REQUIRED
            "dealer_address": "123 test Ave.", // -- everything here below is optional --
            "dealer_contact_name": "Connor Vertus",
            "dealer_dealer_code": "123",
            "dealer_oem": "CVT",
            "dealer_organic_live": true,
            "dealer_paid_live": true,
            "dealer_website_live": true,
            "dealer_phone_number": "555-123-4567",
            "dealer_city": "Vancouver",
            "dealer_country": "CA", // Should it be towards global country coude spec? Like CA, US, etc.
            "dealer_province": "BC",
            // "dealer_state": undefined, - commented out, this will be null in the db that's ok
            "dealer_vms_id": 1,
            "dealer_vms_provider": "test",
            "dealer_website_live_url": "http://test.com",
            "organic_gmb_store_codes": ["123", "456"]
        }
        let response_post = await request.post('/api/v0/dealerships').set('sub', user_id).send(data)
        let response_post2 = await request.post('/api/v0/dealerships').send(data)
        let response_patch = await request.patch('/api/v0/dealerships/' + response_post.body['_id']).set('sub', user_id).send({dealer_address: 'test1'})
        let response_get = await request.get('/api/v0/dealerships/' + response_post.body['_id']).set('sub', user_id)
        let response_get_all = await request.get('/api/v0/dealerships').set('sub', user_id)
        let response_delete = await request.delete('/api/v0/dealerships/' + response_post.body['_id']).set('sub', user_id)
        expect(response_post.status).toBe(200)
        expect(response_post.body.insertedCount).toBe(1)
        expect(response_post2.status).not.toBe(200)
        expect(response_patch.status).toBe(200)
        expect(response_patch.body.modifiedCount).toBe(1)
        expect(response_get.status).toBe(200)
        expect(response_get.body.dealer_address).toBe('test1')
        expect(response_get_all.status).toBe(200)
        console.log(response_get_all.body)
        expect(response_get_all.body.length).toBeGreaterThan(0)
        expect(response_delete.status).toBe(200)
        expect(response_delete.body.nRemoved).toBe(1)
    })

    it('receives at least more than one dealership', async () => {
        let response = await request.get('/api/v0/dealerships').set('sub', user_id)
        expect(response.status).toBe(200)
        expect(response.body.length).toBeGreaterThan(0)
    })

    it('receives at least more than one dealership via param fields user_id and _id', async () => {
        let response = await request.get('/api/v0/dealerships?fields=_id,user_id').set('sub', user_id)
        let data = response.body
        expect(response.status).toBe(200)
        expect(response.body.length).toBeGreaterThan(0)
        for(let x of data) {
            expect(x['_id']).toBeDefined()
            expect(x['user_id']).toBeDefined()
        }
    })

    it('receives all the dealerships and filters through params provided', async () => {
        let response = await request.get('/api/v0/dealerships?filter=').set('sub', user_id)
    })

    it('Fails to POST a new dealership', async () => {
        let data = {
            dealer_address : '',
            dealer_business_name: ''
        }

        let response_post = await request.post('/api/v0/dealerships').set('sub', user_id).send(data)
        expect(response_post.status).not.toBe(200)
    })
    
})