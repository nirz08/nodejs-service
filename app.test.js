describe('auth0_app_tests', () => {
    var server
    var supertest = require('supertest')
    var request
    beforeAll(async () => {
        server = await require('./server')(true)
        request = supertest(server)
    })

    afterAll( async () => {
        await server.close()
        await new Promise(resolve => setTimeout(() => resolve(), 500)); // avoid jest open handle error
    })
    /**
     * Do not pass user id through header
     */
    it('Does not pass without a user id in the header', async () => {
        var response = await request.get('/api/v0/dealerships')
        expect(response.status).toBe(401)
        expect(response.body.length)
    })
    /**
     * Pass user id through header and expect status to be 200
     */
    it('Passes if a user id is in the header', async () => {
        var response = await request.get('/api/v0/dealerships').set('sub', 'auth0|5e542dbf9158130fc9f2fd7c')
        expect(response.status).toBe(200)
    })
});
