# Models

Models are used to validate data to ensure it conforms to the schema of documents in the MongoDB collection. The actual models are inside a json object which is only a set of key/value pairs where the key is the same as the key name in the MongoDB collection and the value assigned to the key is a *primative* type which could be a:

- `Boolean`
- `Number`
- `BigInt`
- `String`
- `Null`
- `Undefined`